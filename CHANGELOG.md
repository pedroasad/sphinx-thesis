# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog][keep-a-changelog] and this project adheres to [Semantic
Versioning][semantic-versioning].


## [Unreleased]

### Added

* [`.bumpversion.cfg`](.bumpversion.cfg) – Configuration file for the [bumpversion][bumpversion] version-tagging package.
* [`.coveragerc`](.coveragerc) – Configuration file for the [Coverage.py][python-coverage] reporting tool.
* [`.gitignore`](.gitignore) – List of files and directories paths/patterns [ignored by Git][gitignore].
* [`.gitlab-ci.yml`](.gitlab-ci.yml) – Continuous integration/deploy configuration ([GitLab CI][gitlab-ci]), featuring:
    * [Security verification][repository-security] of Python dependencies (via [Gitlab Dependency Scanning][gitlab-dependency-scanning])
* [`.pre-commit-config.yaml`](.pre-commit-config.yaml) – Configuration file for the [pre-commit][pre-commit] package, which aids in applying useful [Git Hooks][git-hooks] in team workflows. Includes:
    * Automatic code styling with [Black][pip-black].
* [`CHANGELOG.md`](CHANGELOG.md) – this very history file, which follows the [Keep a Changelog][keep-a-changelog] standard.
* [`LICENSE.txt`](LICENSE.txt) – Copy of the [MIT license][license-mit] (a permissive [open source license][open-source]).
* [`README.md`](README.md) – repository front-page.
* [`src/sphinx_thesis`](src/sphinx_thesis) – Base directory of the example Python package distributed by this repository.
* [`poetry.lock`](poetry.lock) – [Poetry][python-poetry]'s resolved dependency file. Whereas [`pyproject.toml`](pyproject.toml) specifies [*as abstract as possible*][post-setup-vs-requirements] dependencies, this file 
* [`pyproject.toml`](pyproject.toml) – [PEP-517][pep-517]-compliant packaging metadata, configured with the [Poetry][python-poetry] system. Includes, among other information: package qualifiers, version, author, and all of its dependencies. This file replaces the classic [setup.py file][setup.py] found in *classical* Python packaging.
* [`pytest.ini`](pytest.ini) – Configuration file for the [pytest][python-pytest] Python testing framework. It is currently configured to ignore internal warning raised by [Sphinx] and [Docutils].
* [`tests`](tests) – [pytest][python-pytest]-powered test-suite.

[Unreleased]: https://gitlab.com/psa-exe/sphinx-thesis

[bumpversion]: https://github.com/peritus/bumpversion
[git-hooks]: https://git-scm.com/book/en/v2/Customizing-Git-Git-Hooks
[gitignore]: https://git-scm.com/docs/gitignore 
[gitlab-ci]: https://docs.gitlab.com/ee/ci
[gitlab-dependency-scanning]: https://docs.gitlab.com/ee/user/application_security/dependency_scanning/index.html
[gitlab-license-management]: https://docs.gitlab.com/ee/user/application_security/license_management/index.html
[gitlab-license-management]: https://docs.gitlab.com/ee/user/application_security/license_management/index.html
[keep-a-changelog]: https://keepachangelog.com/en/1.0.0/
[license-mit]: https://opensource.org/licenses/MIT
[open-source]: https://opensource.org/
[pep-517]: https://www.python.org/dev/peps/pep-0517/
[pip-black]: https://pypi.org/project/black/
[pip-pytest-cov]: https://pypi.org/project/pytest-cov/
[post-setup-vs-requirements]: https://caremad.io/posts/2013/07/setup-vs-requirement/
[pre-commit]: https://pre-commit.com/
[python-coverage]: https://coverage.readthedocs.io
[python-poetry]: https://github.com/sdispater/poetry
[python-pytest]: https://pytest.org/
[python-setup-post]: https://towardsdatascience.com/10-steps-to-set-up-your-python-project-for-success-14ff88b5d13
[python-sphinx]: https://www.sphinx-doc.org/en/master/index.html
[repository-codecov]: https://codecov.io/gl/pedroasad.com:templates:python/python-app
[repository-security]: https://gitlab.com/psa-exe/sphinx-thesis/security/dashboard
[semantic-versioning]: https://semver.org/spec/v2.0.0.html
[setup.py]: https://docs.python.org/3.7/distutils/setupscript.html


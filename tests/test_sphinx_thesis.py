import pytest
import re
import sphinx_thesis


def test_answer():
    assert sphinx_thesis.answer() == 42


def test_version():
    assert sphinx_thesis.__version__ == "0.1.0"


@pytest.mark.sphinx("html", testroot="example-full")
def test_example_full(app, status, warning):
    app.build()
    html = (app.outdir / "index.html").text()
    assert re.search(r"<h1>sphinx-thesis full example.*?</h1>", html)
    assert re.search(r"<p>Hello, world!</p>", html)

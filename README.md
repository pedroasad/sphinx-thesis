Sphinx Thesis
=============

[![badge-python][badge-python]][python-docs]
[![badge-version][badge-version]][repository-latest-release]

[![badge-mit][badge-mit]][license-mit]
[![badge-black][badge-black]][pip-black]

[![badge-ci-status][badge-ci-status]][repository-master]
[![badge-codecov][badge-codecov]][repository-codecov]
[![badge-ci-security][badge-ci-security]][repository-security]
<!-- [![badge-ci-coverage][badge-ci-coverage]][repository-master] -->

*An extension package for the Sphinx documentation system that allows to create a Ph.D. thesis or academic paper.*

**Disclaimer:** This project is in larval stage. What's documented bellow is more of an intent to implement than an
actual list of features.

**Table of contents:**
[](TOC)
[](TOC)

# Introduction

## Why a Sphinx extension?

[Sphinx][sphinx] is a great technical documentation system. **Period.** [LaTeX][latex] is Royal when it comes to
beautiful document typesetting. **Double period.** Despite these reasons, using either for academic production, like
writing Ph.D. thesis or papers, has pros and cons. While [Sphinx][sphinx] makes it easier to switch between PDF and HTML
output, and is based on wonderful programming language ([Python][python-docs]!), it lacks many of the useful
[LaTeX][latex] semantic constructs available through the thousands of [CTAN][latex-ctan] (subfigures, flexible
glossaries, math macro definitions, *etc.*). [LaTeX][latex], on the other hand, while highly customizable, produces poor
HTML output, results in not very-readable source, and its programmability is terrible. The original inspiration for this
approach came from a similar project: [sphinxtr][sphinxtr]. 

## Features

This package attempts to hit a sweet spot for those familiar with the [Sphinx][sphinx] documentation system. Through a
collection of additional [directives][sphinx-docs-directives] and [roles][sphinx-docs-roles], it will **(hopefully, when
ready)** provide functionality that is missing from vanilla [Sphinx][sphinx] and its core extensions, like:

* Subfigures (with consistent layout, numbering, and indexing)
* Appendix numbering
* Additional citation commands (like [LaTeX][latex] `citet`, and `citep`, for example)
* Math macro definitions
* Integrated rendering of [Matplotlib][matplotlib] plots, [Pandas][pandas] data-frames, and [Numpy][numpy] record arrays
* Better glossary, and acronym definitions, with math symbol support (like the `glossaries` [LaTeX][latex] package)
* Native support for the [CoppeTeX][coppetex] package (my [university][ufrj-coppe]'s own [LaTeX][latex] class)

## Motivation and usefulness

You should be aware that I developed this package for personal reasons, so it may, or may not be, suitable for your use
case, depending on your background and goals. My motivation for building this package during my Ph.D. was threefold:

* Learning more about [writing Sphinx extensions][sphinx-docs-extensions],
* Easily switch between PDF and HTML output, while keeping my [institution][ufrj-coppe]'s  required formatting, and
* Being able to transplant unused extra notes to my [personal website/blog/knowledge base][mysite], which is already
  [Sphinx][sphinx]-based

A fourth reason would be that HTML seemed like the proper intermediate format for a growing thesis. PDFs do make sense
when it comes the time to hand in the final, revised version of you thesis to your university's library. However, during
development, HTML is much more readable and easier to navigate, both for who's writing and for who's reviewing the text.
I chose not to fork the related [sphinxtr][sphinxtr] project because I wanted to learn everything from the ground up,
and because the project had been abandoned for over 2 years when I got started in April, 2019.  

Since the beginning, I knew this could be useful to others, so I tried to adhere to best practices of coding, testing, 
and documentation, but, as I said, this package usability's depends on you background. Personally, I find this is going
to be most useful to people who are using [Python][python-docs] (and perhaps [Jupyter][jupyter]) to write their
experiments, even if they haven't used [Sphinx][sphinx] before.     

# Usage

Assuming you are already familiar and setup with a [Sphinx][sphinx], install the latest version with

```bash
pip install sphinx-thesis
```

then add `'sphinx_thesis'` to the `extensions` list of you `conf.py` file, for example:

```python
extensions = [
    # ...
    'sphinx_thesis',
]
``` 
   
   
# Future directions

* Complete the Sphinx extension tutorials
* Set up Sphinx documentation
* Set up Tox/py.test test suite
* Publish on PyPI

# Contributing

Some technologies used in this project that you should be aware of, in order to fork this repository or (hopefully)
contribute:

* Dependency management and resolution with [Poetry][python-poetry]
* Automatic code styling with [Black][pip-black]
* [Pytest][python-pytest]-based test suite with [Codecov][codecov]-powered [coverage reports][repository-codecov]
* [Security verification][repository-security] of Python dependencies (via [Gitlab Dependency Scanning][gitlab-dependency-scanning])
* Version bumping/tagging with [bumpversion][pip-bumpversion] 

Further references for beginners:

* [How to package your Python code](https://python-packaging.readthedocs.io/en/latest/index.html)
* Sphinx and Docutils documentation
    * [Creating reStructuredText Directives](http://docutils.sourceforge.net/docs/howto/rst-directives.html)
    * [Creating reStructuredText Interpreted Text Roles](http://docutils.sourceforge.net/docs/howto/rst-roles.html)
    * [Developing extensions for Sphinx (API)](https://www.sphinx-doc.org/en/master/extdev/)
    * [Sphinx extension tutorials](https://www.sphinx-doc.org/en/master/development/tutorials/index.html)

## Installation

System requirements:

* Python 3.6
* [Poetry][python-poetry] 0.12

Instructions follow:

1. Create a [virtual environment][virtualenv] and install dependencies. Two of the most common options are:

    * Letting [Poetry][python-poetry] create the virtual environment and install dependencies for you (the default if
      you have just installed Poetry):
      
      ```bash
      poetry install  # This will create a virtual environment in a new subdirectory 
                      # of $(poetry config settings.virtualenvs.path) 
      ```
      
      An interesting variation is telling Poetry to create the environment inside the project's root folder by running
      
      ```bash
      poetry config settings.virtualenvs.in-project true
      ```
      
      before the `install` command.
      
    * If you have choosen to turn off automatic environment creation using
    
      ```bash
      poetry config settings.virtualenvs.create false
      ```

      then the following steps should suffice:
      
      ```bash
      virtualenv -p $(which python3.7) .venv
      source venv/bin/activate.sh
      poetry install
      ```
      
      In this case, keep in mind that you need to activate the environment using the `source` command before issuing any
      Poetry commands. You may choose a name for your virtual environment other than `.venv`, but this one is already on
      [`.gitignore`](.gitignore). 
      
1. **(Optional)** Set up [pre-commit][pre-commit]. It is already included as *dev*-dependency in
   [pyproject.toml](pyproject.toml) and a [.pre-commit-config.yaml](.pre-commit-config.yaml) file is provided. Hence,
   you just need:
   
   ```bash
   pre-commit install
   ```
   
## Running the tests

Provided that the virtual environment is active, and dependencies have been installed, install the package and run the
tests with

```bash
poetry install
pytest
```

This already produces a code coverage report for the [`app`](src/sphinx_thesis/app) package, stores it in a `.coveragerc` file and prints it.

## Inventory

* [`.bumpversion.cfg`](.bumpversion.cfg) – Configuration file for the [bumpversion][bumpversion] version-tagging package.
* [`.coveragerc`](.coveragerc) – Configuration file for the [Coverage.py][python-coverage] reporting tool.
* [`.gitignore`](.gitignore) – List of files and directories paths/patterns [ignored by Git][gitignore].
* [`.gitlab-ci.yml`](.gitlab-ci.yml) – Continuous integration/deploy configuration ([GitLab CI][gitlab-ci]).
* [`.pre-commit-config.yaml`](.pre-commit-config.yaml) – Configuration file for the [pre-commit][pre-commit] package, which aids in applying useful [Git Hooks][git-hooks] in team workflows.
* [`CHANGELOG.md`](CHANGELOG.md) – history of changes, which follows the [Keep a Changelog][keep-a-changelog] standard.
* [`LICENSE.txt`](LICENSE.txt) – Copy of the [MIT license][license-mit] (a permissive [open source license][open-source]).
* [`README.md`](README.md) – Source file for this very front-page!
* [`src/sphinx_thesis`](src/sphinx_thesis/app) – Base directory of the example Python package distributed by this repository.
* [`poetry.lock`](poetry.lock) – [Poetry][python-poetry]'s resolved dependency file. Whereas [`pyproject.toml`](pyproject.toml) specifies [*as abstract as possible*][post-setup-vs-requirements] dependencies, this file 
* [`pyproject.toml`](pyproject.toml) – [PEP-517][pep-517]-compliant packaging metadata, configured with the [Poetry][python-poetry] system. Includes, among other information: package qualifiers, version, author, and all of its dependencies. This file replaces the classic [setup.py file][setup.py] found in *classical* Python packaging.
* [`pytest.ini`](pytest.ini) – Configuration file for the [pytest][python-pytest] Python testing framework. It is currently configured to ignore internal warning raised by [Sphinx] and [Docutils].
* [`tests`](tests) – [pytest][python-pytest]-powered test-suite.

---

*– Inspired by [sphinxtr][sphinxtr]*  
*– Powered by [Gitlab CI][gitlab-ci]*  

[badge-black]: https://img.shields.io/badge/code%20style-Black-black.svg
[badge-ci-coverage]: https://gitlab.com/psa-exe/sphinx-thesis/badges/master/coverage.svg
[badge-ci-security]: https://img.shields.io/badge/security-Check%20here!-yellow.svg
[badge-ci-status]: https://gitlab.com/psa-exe/sphinx-thesis/badges/master/pipeline.svg
[badge-codecov]: https://codecov.io/gl/psa-exe/sphinx-thesis/branch/master/graph/badge.svg
[badge-mit]: https://img.shields.io/badge/license-MIT-blue.svg
[badge-python]: https://img.shields.io/badge/Python-%E2%89%A53.6-blue.svg
[badge-version]: https://img.shields.io/badge/version-0.1.0%20(pre%20alpha)-orange.svg
[bumpversion]: https://github.com/peritus/bumpversion
[codecov]: https://codecov.io
[coppetex]: http://coppetex.sourceforge.net/
[Docutils]: http://docutils.sourceforge.net/docs/
[git-hooks]: https://git-scm.com/book/en/v2/Customizing-Git-Git-Hooks
[github-sphinxtr]: https://github.com/jterrace/sphinxtr
[gitignore]: https://git-scm.com/docs/gitignore 
[gitlab-ci]: https://docs.gitlab.com/ee/ci
[gitlab-dependency-scanning]: https://docs.gitlab.com/ee/user/application_security/dependency_scanning/index.html
[jupyter]: https://jupyter.org/
[keep-a-changelog]: https://keepachangelog.com/en/1.0.0/
[latex]: https://www.latex-project.org/
[latex-ctan]: https://ctan.org/
[license-mit]: https://opensource.org/licenses/MIT
[matplotlib]: https://matplotlib.org/
[mysite]: https://pedroasad.com
[numpy]: https://www.numpy.org/
[open-source]: https://opensource.org/
[pandas]: https://pandas.pydata.org/
[pep-517]: https://www.python.org/dev/peps/pep-0517/
[pip-black]: https://pypi.org/project/black/
[pip-bumpversion]: https://pypi.org/project/bumpversion/
[pre-commit]: https://pre-commit.com/
[pip-pytest-cov]: https://pypi.org/project/pytest-cov/
[post-setup-vs-requirements]: https://caremad.io/posts/2013/07/setup-vs-requirement/
[python-coverage]: https://coverage.readthedocs.io
[python-docs]: https://docs.python.org/3.6/
[python-poetry]: https://github.com/sdispater/poetry
[python-pytest]: https://pytest.org/
[python-setup-post]: https://towardsdatascience.com/10-steps-to-set-up-your-python-project-for-success-14ff88b5d13
[repository]: https://gitlab.com/psa-exe/sphinx-thesis
[repository-codecov]: https://codecov.io/gl/psa-exe/sphinx-thesis
[repository-latest-release]: https://gitlab.com/psa-exe/sphinx-thesis
[repository-master]: https://gitlab.com/psa-exe/sphinx-thesis/commits/master
[repository-security]: https://gitlab.com/psa-exe/sphinx-thesis/security/dashboard
[setup.py]: https://docs.python.org/3.7/distutils/setupscript.html
[sphinx]: http://sphinx-doc.org/
[sphinx-docs-directives]: https://www.sphinx-doc.org/en/master/usage/restructuredtext/directives.html
[sphinx-docs-extensions]: https://www.sphinx-doc.org/en/latest/development/tutorials/index.html
[sphinx-docs-roles]: https://www.sphinx-doc.org/en/master/usage/restructuredtext/roles.html
[sphinxtr]: https://github.com/jterrace/sphinxtr
[ufrj-coppe]: http://www.coppe.ufrj.br/en
[virtualenv]: https://virtualenv.pypa.io/en/latest/
